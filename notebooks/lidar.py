"""
Class to read MakerHawk TFmini (basic model) sensor.
"""

import serial
import time

SENSOR_PORT = '/dev/serial0'

class lidar():
    
    def __init__(self, sensor_port : str):
        self._ser = serial.Serial(sensor_port, 115200, timeout=1)
        
        # Initialise 
        self._ser.write(bytes(b'B'))
        self._ser.write(bytes(b'W'))
        self._ser.write(bytes(2))
        self._ser.write(bytes(0))
        self._ser.write(bytes(0))
        self._ser.write(bytes(0))
        self._ser.write(bytes(1))
        self._ser.write(bytes(6))
        
    def read_lidar(self):
        """
        Take a single reading from the lidar.
        """
        Dist_Total = 0
        while(self._ser.in_waiting >= 9):
            
            
            if((b'Y' == self._ser.read()) and ( b'Y' == self._ser.read())):
            
                Dist_L = self._ser.read()
                Dist_H = self._ser.read()
                Dist_Total = (ord(Dist_H) * 256) + (ord(Dist_L))
                for i in range (0,5):
                    self._ser.read()
        return Dist_Total