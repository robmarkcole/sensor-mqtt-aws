"""
Class to read data from 2 lidar and log to csv.

Just ctrl + c to kill. 
"""
from datetime import datetime
import csv
import serial
import time
import json

# Setup hardware
LIDAR_1 = '/dev/ttyUSB0'
LIDAR_2 = '/dev/ttyUSB1'

# Setup measurement parameters
CSV_COLUMNS = ['time', 'LIDAR_1', 'LIDAR_2'] # the columns to use in the csv

CAPTURE_HZ = 20 # this appears to give good resolution.


def get_now():
    """Return formatted now time string"""
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")


def csv_writer(data, csv_file_path):
    """
    Append data list to a CSV file path.
    """
    with open(csv_file_path, "a") as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerow(data)
    return None


def generate_csv_file(columns):
    """
    create a new csv file with appropriate columns.
    
    columns : a list of columns titles.
    """
    
    csv_path = "data_" + datetime.now().strftime("%Y-%m-%d_%H:%M") + ".csv"
    csv_writer(columns, csv_path) # Create first line in file
    print("Created file {}".format(csv_path))
    return csv_path

    
class lidar():
    def __init__(self, sensor_port):
        self._ser = serial.Serial(sensor_port, 115200, timeout=1)
        
        # Initialise 
        self._ser.write(bytes(b'B'))
        self._ser.write(bytes(b'W'))
        self._ser.write(bytes(2))
        self._ser.write(bytes(0))
        self._ser.write(bytes(0))
        self._ser.write(bytes(0))
        self._ser.write(bytes(1))
        self._ser.write(bytes(6))
        
    def read_lidar(self):
        """
        Take a single reading from the lidar.
        """
        Dist_Total = 0
        while(self._ser.in_waiting >= 9):
            
            
            if((b'Y' == self._ser.read()) and ( b'Y' == self._ser.read())):
            
                Dist_L = self._ser.read()
                Dist_H = self._ser.read()
                Dist_Total = (ord(Dist_H) * 256) + (ord(Dist_L))
                for i in range (0,5):
                    self._ser.read()
        return Dist_Total

# Func to loop over i READINGS and capture data from 2 lidar, write to csv
def main():
    lidar_1 = lidar(LIDAR_2)
    lidar_2 = lidar(LIDAR_1)
    csv_file_path = generate_csv_file(CSV_COLUMNS) # Create file
    
    # Loop and capture data
    while True:
        reading_time = get_now()
        reading_1 = lidar_1.read_lidar()
        reading_2 = lidar_2.read_lidar()
        data = [reading_time, reading_1, reading_2]
        
        csv_writer(data, csv_file_path) # Write data
        
        print(json.dumps(data))
        time.sleep(1/CAPTURE_HZ)
                
if __name__ == "__main__":
    main()