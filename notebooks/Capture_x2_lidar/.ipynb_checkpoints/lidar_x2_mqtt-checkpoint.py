"""
Class to read data from 2 lidar and publish via MQTT.

Just ctrl + c to kill. 
"""
from datetime import datetime
import csv
import serial
import time
import json
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish 

# Setup hardware
LIDAR_1 = '/dev/ttyUSB0'
LIDAR_2 = '/dev/ttyUSB1'
CAPTURE_HZ = 20 # this appears to give good resolution.


### MQTT 
broker = "192.168.66.91" # mac desktop
port = 1883
LIDAR_1_topic ='lidar_1'
LIDAR_2_topic ='lidar_2'

def on_connect(client, userdata, flags, rc):
    print("CONNACK received with code {}".format(rc))
    if rc==0:
        print("connected OK")
    else:
        print("Bad connection Returned code=",rc)

def on_publish(client, userdata, mid):
    pass
    # print(str(mid))   


### Program code
def get_now():
    """Return formatted now time string"""
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
  
class lidar():
    def __init__(self, sensor_port):
        self._ser = serial.Serial(sensor_port, 115200, timeout=1)
        
        # Initialise 
        self._ser.write(bytes(b'B'))
        self._ser.write(bytes(b'W'))
        self._ser.write(bytes(2))
        self._ser.write(bytes(0))
        self._ser.write(bytes(0))
        self._ser.write(bytes(0))
        self._ser.write(bytes(1))
        self._ser.write(bytes(6))
        
    def read_lidar(self):
        """
        Take a single reading from the lidar.
        """
        Dist_Total = 0
        while(self._ser.in_waiting >= 9):
            
            
            if((b'Y' == self._ser.read()) and ( b'Y' == self._ser.read())):
            
                Dist_L = self._ser.read()
                Dist_H = self._ser.read()
                Dist_Total = (ord(Dist_H) * 256) + (ord(Dist_L))
                for i in range (0,5):
                    self._ser.read()
        return Dist_Total

def main():
    # Init mqtt
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_publish = on_publish

    #client.username_pw_set(username, password)
    client.connect(broker, port=port)
    client.loop_start()
    
    # Init hardware
    lidar_1 = lidar(LIDAR_2)
    lidar_2 = lidar(LIDAR_1)
    
    # Loop and capture data
    while True:
        # reading_time = get_now()
        reading_1 = lidar_1.read_lidar()
        reading_2 = lidar_2.read_lidar()
        print((reading_1, reading_2))
        
        client.publish(LIDAR_1_topic, str(reading_1))
        client.publish(LIDAR_2_topic, str(reading_1))
        time.sleep(1/CAPTURE_HZ)
                
if __name__ == "__main__":
    main()