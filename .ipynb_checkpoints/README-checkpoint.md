# sensor-mqtt-aws

Project scope: demo integration of Lidar sensor on raspberry pi, publish data using MQTT, process on AWS IoT, expose to BlockDox.

### AWS IoT
* [AwS IoT](https://docs.aws.amazon.com/iot) & [raspberry pi tutorial](https://docs.aws.amazon.com/iot/latest/developerguide/sdk-tutorials.html#iot-sdk-create-thing)
* [Overview article](https://iotbytes.wordpress.com/starting-with-aws-iot/)
* Devices (e.g. a pi) are referred to as 'things' or 'devices' and added to the 'registry'.
* The state of the thing is represented on the server as the [Thing Shadow](https://docs.aws.amazon.com/iot/latest/developerguide/iot-device-shadows.html), and is a json object up to 5 deep, and can be queried via MQTT or REST
* [Platform limits here](https://docs.aws.amazon.com/general/latest/gr/aws_service_limits.html#limits_iot)

### Auth
* Each devices is granted a certificate, Communication between your device and AWS IoT is protected through the use of X.509 certificates. When working with a device you will need to copy the private key and rootCA certificate onto your device.
* You create 'policies' which govern the behaviour of things, and attach these to devices
* MQTT -> Use topics to enable applications and things to get, update, or delete the state information for a Thing  
* To get a valid `root-CA.crt` certificate I followed [this guide](https://iotbytes.wordpress.com/mqtt-with-aws-iot-using-python-and-paho/) and downloaded a 'connection kit' which included the valid certificate

### Rules
* [Rules](https://docs.aws.amazon.com/iot/latest/developerguide/iot-rules.html) give your devices the ability to interact with AWS services. 
* Using basic ingest you can forward messages on to other AWS services without incurring a cost. To use Basic Ingest, you send messages from your devices or applications with topic names that start with `$aws/rules/rule-name` as their first three levels, where `rule-name` is the name of your AWS IoT Rule to trigger.
* Rules are defined [using a SQL-like syntax](https://docs.aws.amazon.com/iot/latest/developerguide/iot-create-rule.html), and defined in .json documents which are uploaded using the aws-cli.
* Common uses for rules are to invoke a lambda function or insert data into a dynamoDB table. [The full list is here.](https://docs.aws.amazon.com/iot/latest/developerguide/iot-rule-actions.html). Options include [piping the data to salesforce](https://www.salesforce.com/uk/products/iot-cloud/why-salesforce/#)

### Python Api
* [aws-iot-device-sdk-python](https://github.com/aws/aws-iot-device-sdk-python)
* [Example using regular paho library](https://github.com/mariocannistra/python-paho-mqtt-for-aws-iot/blob/master/awsiotpub.py)

